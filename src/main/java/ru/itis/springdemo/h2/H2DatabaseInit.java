package ru.itis.springdemo.h2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.itis.springdemo.person.Person;
import ru.itis.springdemo.person.PersonRepository;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Bulat Giniyatullin
 * @since 04.04.2021
 */
@Profile("h2")
@Component
public class H2DatabaseInit implements ApplicationRunner {
    private final PersonRepository personRepository;

    @Autowired
    public H2DatabaseInit(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        if (personRepository.count() == 0) {
            for (int i = 0; i < 5; i++) {
                Person person = Person.builder()
                        .name(UUID.randomUUID().toString())
                        .birthDate(new Date(ThreadLocalRandom.current().nextInt() * 1000L))
                        .build();
                personRepository.save(person);
            }
        }
    }
}
