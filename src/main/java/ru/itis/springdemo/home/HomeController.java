package ru.itis.springdemo.home;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Bulat Giniyatullin
 * @since 04.04.2021
 */
@RestController
@RequestMapping("/")
public class HomeController {
    @GetMapping
    public String homePage() {
        return "It's working!";
    }
}
