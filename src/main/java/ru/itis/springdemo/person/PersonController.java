package ru.itis.springdemo.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Bulat Giniyatullin
 * @since 04.04.2021
 */
@RestController
@RequestMapping("/persons")
public class PersonController {
    private final PersonsService personsService;

    @Autowired
    public PersonController(PersonsService personsService) {
        this.personsService = personsService;
    }

    @GetMapping
    public List<Person> listPersons() {
        return personsService.listPersons();
    }

    @GetMapping("/{personId}")
    public Person getPerson(@PathVariable Long personId) {
        return personsService.findPerson(personId);
    }
}
