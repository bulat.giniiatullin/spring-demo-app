package ru.itis.springdemo.person;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Bulat Giniyatullin
 * @since 04.04.2021
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
}
