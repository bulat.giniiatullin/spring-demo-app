package ru.itis.springdemo.person;

import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.springdemo.commons.exception.ResourceNotFoundException;

import java.util.List;

/**
 * @author Bulat Giniyatullin
 * @since 04.04.2021
 */
@Service
public class PersonsService {
    private final PersonRepository personRepository;

    @Autowired
    public PersonsService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> listPersons() {
        return IterableUtils.toList(personRepository.findAll());
    }

    public Person findPerson(Long personId) {
        return personRepository.findById(personId)
                .orElseThrow(ResourceNotFoundException::new);
    }
}
